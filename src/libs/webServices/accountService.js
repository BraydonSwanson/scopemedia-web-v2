'use strict'
import axios from 'axios'
import store from '@/store'
import serverUrls from '@/libs/webServices/serverUrls.js'

export default {

  /**
   * Post Login request
   * @param username
   * @param password
   * @returns {*|axios.Promise}
   */
  async authenticate (username, password) {
    try {
      var response = await axios({
        method: 'POST',
        url: `${serverUrls.API_SERVER}/api/authenticate`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          username: username,
          password: password
        }
      })
      if (response.status === 200 && response.data['id_token']) {
        return response.data['id_token']
      }
    } catch (error) {
      console.error(error)
    }
    throw new Error('Authentication failed')
  },


  /**
   *
   * @param username
   * @param password
   * @param email
   * @returns {*|axios.Promise}
   */
  register (username, password, email, firstName, lastName, company, country) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/api/register`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        login: username,
        password: password,
        email: email,
        firstName: firstName,
        lastName: lastName,
        company: company,
        country: country,
        langKey: 'en'
      }
    })
  },


  /**
   *
   * @param pw
   * @returns {*|axios.Promise}
   *
   */
  changePassword (password) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/api/account/change_password`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      },
      data: password
    })
  },

  /**
   * Confirm email
   * @param key
   * @returns {*|axios.Promise}
   */
  confrimEmail (key) {
    return axios({
      method:'GET',
      url: `${serverUrls.API_SERVER}/api/activate?key=${key}`
    })
  },

  /**
   * Post reset password request
   * @param key
   * @param pw
   * @returns {*|axios.Promise}
   */
  resetPw (key, pw) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/api/account/reset_password/finish`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        key: key,
        newPassword: pw
      }
    })
  },


  /**
   * Get logged in user's info
   * IF no user logged in, @returns Promise reject
   * Else @returns {*|axios.Promise}
   */
  getAccount () {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/api/account`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  /**
   * Update user info
   * @param newUserData
   * IF no user logged in, @returns Promise reject
   * Else @returns {*|axios.Promise}
   */
  saveAccount (newUserData) {
    return axios({
      method: 'post',
      url: `${serverUrls.API_SERVER}/api/account`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      },
      data: newUserData
    })
  },

  /**
   * Get API keys
   * @returns {*|axios.Promise}
   */
  async getApplicationList () {
    try {
      const response = await axios({
        method: 'GET',
        url: `${serverUrls.API_SERVER}/api/api-keys`,
        headers: {
          'Authorization': `Bearer ${store.state.accessToken}`
        }
      })
      if (response && response.data) {
        return response.data
      }
    } catch (error) {
      console.error(error)
    }

    throw new Error('Failed loading application list')
  },

  /**
   * Generate a new API Key
   * @param application
   * @param type
   * @returns {*|axios.Promise}
   */
  createApplication (application, type='PRODUCTION') {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/api/api-keys/generate`,
      headers: {
        'Authorization': `Bearer ${store.state.accessToken}`
      },
      data: {
        application: application,
        type: type
      }
    })
  },

  /**
   *
   * Delete an existed API
   * @param id
   * @returns {*|axios.Promise}
   */
  deleteApplication (id) {
    return axios({
      method: 'DELETE',
      url: `${serverUrls.API_SERVER}/api/api-keys/${id}`,
      headers: {
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  /**
   *
   * Post billing information
   * @param billing
   * @param planType
   * @param product
   * @returns {*|axios.Promise}
   */
  postSubscription (billing, planType, product) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/api/subscription`,
      headers: {
        'Authorization': `Bearer ${store.state.accessToken}`
      },
      data: {
        'billing': billing,
        'planType': planType,
        'product': product
      }
    })
  },


  /**
   * Get Usage Infomation
   * @param productName
   * @param showDetail
   * @returns {*|axios.Promise}
   */
  getUsage (productName, showDetail) {
    let url = `${serverUrls.API_SERVER}/api/usage/monthly?`
    showDetail ?
      url += 'showDetail=true' :
      url += 'showDetail=false'
    productName ?
      url += `product=${productName}` :
      url
    return axios({
      method: 'GET',
      url: url,
      headers: {
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  /**
   * Get S3 Token
   * @returns {*|axios.Promise}
   */
  getS3Token () {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/api/s3token?client_id=demo&client_secret=demosecret`
    })
  },

  getS3Images (url, token, signature, timestamp) {
    return axios({
      method: 'GET',
      url: url,
      headers: {
        'x-amz-security-token': token,
        'Authorization': signature,
        'x-amz-date': timestamp
      }
    })
  },

  getInvoices (isAdmin) {
    let url = isAdmin ?
      `${serverUrls.API_SERVER}/api/admin/invoices` :
      `${serverUrls.API_SERVER}/api/invoices`
    return axios({
      method: 'GET',
      url: url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  }
}
