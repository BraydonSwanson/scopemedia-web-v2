export default class Tile {
  constructor ({level, col, row, url, ctx, tileSize, parent}) {
    this.level = level
    this.col = col
    this.row = row
    this.image = null
    this.url = url
    this.ctx = ctx
    this.imageRequested = false
    this.tileSize = tileSize

    var graphingTileSize = tileSize / Math.pow(2, level)
    this.graphing = {
      x: col * graphingTileSize,
      y: row * graphingTileSize,
      tileSize: graphingTileSize
    }
    this.parent = parent

    // if this is the parent node, load the image immediately so we can use it as placeholder when child node is downloading image
    if (level === 0) {
      this.loadImage()
    }
  }

  loadImage () {
    var img = new Image()
    img.src = this.url
    img.onload = () => {
      // assign back to the objact after image loaded.
      // before that, we should ask parent node to provide image as placeholder
      this.image = img
    }

    // mark the tile so we don't
    this.imageRequested = true
  }

  drawImage (zooming) {
    if (this.image) {
      this.ctx.beginPath()
      this.ctx.drawImage(this.image, this.graphing.x, this.graphing.y, this.graphing.tileSize, this.graphing.tileSize)

      // for debug
      // this.drawDebugIndicator('#f00', this);
    } else if (this.parent) {
      // ask parent node to provide image as placeholder
      this.parent.drawForChild(this)
    } else {
      // this is the root already, no image available, just fill black
      this.ctx.fillStyle = '#000'
      this.ctx.fillRect(this.graphing.x, this.graphing.y, this.graphing.tileSize, this.graphing.tileSize)
    }

    // download the image if haven't done
    // but if zooming is detected, skip this frame because user may ends up in another zoom level. For example, if user zoom from 1 to 4, we don't want to load images at level 2 and 3. Save traffic to load at level 4

    if (!this.imageRequested && !zooming) {
      this.loadImage()
    }
  }

  /*
   * this is identical to drawImage(), but it doesn't draw itself
   * instead, it draws in the area which the child asked
   */
  drawForChild (child) {
    if (this.image) {
      // find out the scale from child to parent
      let relativeScale = Math.pow(2, (child.level - this.level))
      this.ctx.beginPath()
      // based on the scale, crop out a section from parent and draw at requested area
      this.ctx.drawImage(this.image, this.tileSize * ((child.col / relativeScale) % 1), this.tileSize * ((child.row / relativeScale) % 1), this.tileSize / relativeScale, this.tileSize / relativeScale, child.graphing.x, child.graphing.y, child.graphing.tileSize, child.graphing.tileSize)

      // for debug
      // this.drawDebugIndicator('#ff0', child);
    } else if (this.parent) {
      this.parent.drawForChild(child)
    } else {
      this.ctx.fillStyle = '#000'
      this.ctx.fillRect(child.graphing.x, child.graphing.y, child.graphing.tileSize, child.graphing.tileSize)
    }
  }

  drawDebugIndicator (color, tile) {
    this.ctx.textAlign = 'center'
    this.ctx.textBaseline = 'middle'
    this.ctx.lineWidth = 8 / Math.pow(2, tile.level)
    this.ctx.font = 50 / Math.pow(2, tile.level) + 'px Arial'
    this.ctx.fillStyle = this.ctx.strokeStyle = color
    this.ctx.strokeRect(tile.graphing.x, tile.graphing.y, tile.graphing.tileSize, tile.graphing.tileSize)
    this.ctx.fillText(this.level, tile.graphing.x + tile.graphing.tileSize / 2, tile.graphing.y + tile.graphing.tileSize / 2)
  }
}
