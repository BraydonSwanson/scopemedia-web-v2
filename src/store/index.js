/**
 * Created by Alex on 2017-03-21.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import accountService from '@/libs/webServices/accountService.js'
import imageDemo from './imageDemo.js'
import ScopeVision from './ScopeVision.js'
import ScopeCheck from './ScopeCheck.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: { imageDemo, ScopeVision, ScopeCheck },
  state: {
    userInfo: null,
    accessToken: window.localStorage.getItem('_scopeMedia_dashboard_accessToken')
  },

  getters: {
    isAdmin (state) {
      return state.userInfo !== null && state.userInfo.authorities && state.userInfo.authorities.indexOf('ROLE_ADMIN') >= 0
    },
    hasUserInfo (state) {
      return state.userInfo !== null
    },
    hasAccessToken (state) {
      return state.accessToken !== null
    }
  },

  actions:{
    setAccessToken ({ commit}, { newAccessToken }) { commit('SET_ACCESS_TOKEN', { newAccessToken} ) },
    removeAccessToken ({ commit }) { commit('REMOVE_ACCESS_TOKEN') },
    setUser ({ commit}, { newUser }) { commit('SET_USER_INFO', { newUser} ) },
    removeUser ({ commit }) { commit('REMOVE_USER_INFO') },
    async getAndSetUserInfo ({ dispatch, commit }) {
      try {
        var response = await accountService.getAccount()
        if (response.status === 200 && response.data) {
          commit('SET_USER_INFO', {newUser: response.data})
          return true
        }
      } catch (error) {
        console.error(error)
      }
      dispatch('logout')
      throw new Error('Unable to get user profile')
    },
    // this is triggered if user logout mannually, or access token has expired
    // App.vue watches userInfo. Once hasUserInfo is changed from true to false, it redirects user to login page
    logout ({ dispatch, commit }) {
      commit('REMOVE_ACCESS_TOKEN')
      commit('REMOVE_USER_INFO')
      dispatch('ScopeVision/remove')
      dispatch('ScopeCheck/remove')
    }
  },

  mutations:{
    SET_ACCESS_TOKEN (state, { newAccessToken }) {
      state.accessToken = newAccessToken
      window.localStorage.setItem('_scopeMedia_dashboard_accessToken', newAccessToken)
    },

    REMOVE_ACCESS_TOKEN (state) {
      state.accessToken = null
      window.localStorage.removeItem('_scopeMedia_dashboard_accessToken')
    },

    SET_USER_INFO (state, { newUser }) {
      state.userInfo = newUser
    },

    REMOVE_USER_INFO (state) {
      state.userInfo = null
    }
  }
})

export default store
